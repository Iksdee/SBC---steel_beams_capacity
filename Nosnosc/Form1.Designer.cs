﻿namespace Nosnosc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_L = new System.Windows.Forms.TextBox();
            this.textBox_P = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_Q = new System.Windows.Forms.TextBox();
            this.textBox_M = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.TypeOfProfiles = new System.Windows.Forms.ComboBox();
            this.selectSection = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ClassOfSteal = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.LabelMc = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label19 = new System.Windows.Forms.Label();
            this.Wyniki = new System.Windows.Forms.GroupBox();
            this.LabelVc = new System.Windows.Forms.Label();
            this.LabelSc = new System.Windows.Forms.Label();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxNr = new System.Windows.Forms.TextBox();
            this.textBoxVc = new System.Windows.Forms.TextBox();
            this.textBoxNc = new System.Windows.Forms.TextBox();
            this.textBoxMc = new System.Windows.Forms.TextBox();
            this.richTextBoxNrd = new System.Windows.Forms.RichTextBox();
            this.richTextBoxVrd = new System.Windows.Forms.RichTextBox();
            this.richTextBoxNrds = new System.Windows.Forms.RichTextBox();
            this.richTextBoxMcrd = new System.Windows.Forms.RichTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_S = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Wyniki.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.IntegralHeight = false;
            this.comboBox1.ItemHeight = 120;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox1.Location = new System.Drawing.Point(49, 38);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(270, 126);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.comboBox1_DrawItem);
            this.comboBox1.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.comboBox1_MeasureItem);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Sila_skupiona.png");
            this.imageList1.Images.SetKeyName(1, "Sila_rozłożona.png");
            this.imageList1.Images.SetKeyName(2, "Po_trojkacie.png");
            this.imageList1.Images.SetKeyName(3, "Moment.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Schemat statyczny belki";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Siła Skupiona                       P [kN] = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Rozpiętość belki                    L [m] =";
            // 
            // textBox_L
            // 
            this.textBox_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_L.Location = new System.Drawing.Point(249, 17);
            this.textBox_L.Name = "textBox_L";
            this.textBox_L.Size = new System.Drawing.Size(40, 23);
            this.textBox_L.TabIndex = 4;
            this.textBox_L.Text = "10";
            this.textBox_L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_L_KeyPress);
            // 
            // textBox_P
            // 
            this.textBox_P.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_P.Location = new System.Drawing.Point(249, 46);
            this.textBox_P.Name = "textBox_P";
            this.textBox_P.Size = new System.Drawing.Size(40, 23);
            this.textBox_P.TabIndex = 5;
            this.textBox_P.Text = "10";
            this.textBox_P.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_P_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Siła rozłożona                   q [kN/m] = ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(6, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Moment                             M [kNm] = ";
            // 
            // textBox_Q
            // 
            this.textBox_Q.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Q.Location = new System.Drawing.Point(249, 75);
            this.textBox_Q.Name = "textBox_Q";
            this.textBox_Q.Size = new System.Drawing.Size(40, 23);
            this.textBox_Q.TabIndex = 8;
            this.textBox_Q.Text = "0";
            this.textBox_Q.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Q_KeyPress);
            // 
            // textBox_M
            // 
            this.textBox_M.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_M.Location = new System.Drawing.Point(249, 104);
            this.textBox_M.Name = "textBox_M";
            this.textBox_M.Size = new System.Drawing.Size(40, 23);
            this.textBox_M.TabIndex = 9;
            this.textBox_M.Text = "0";
            this.textBox_M.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_M_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_S);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox_L);
            this.groupBox1.Controls.Add(this.textBox_P);
            this.groupBox1.Controls.Add(this.textBox_Q);
            this.groupBox1.Controls.Add(this.textBox_M);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(15, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 163);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane wejściowe ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.Location = new System.Drawing.Point(6, 132);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(241, 15);
            this.label32.TabIndex = 10;
            this.label32.Text = "Siła ściskająca                     P [kN] = ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(346, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 119);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wyniki - max siły wewnętrzne";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(191, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 15);
            this.label14.TabIndex = 8;
            this.label14.Text = "kN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(191, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 15);
            this.label13.TabIndex = 7;
            this.label13.Text = "kN";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(191, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 15);
            this.label12.TabIndex = 6;
            this.label12.Text = "kNm";
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(115, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 20);
            this.label11.TabIndex = 5;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(115, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 20);
            this.label10.TabIndex = 4;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(115, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 20);
            this.label9.TabIndex = 2;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(3, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Siła tnąca B";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(3, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Siła tnąca A";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(3, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Moment";
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(636, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 61);
            this.button1.TabIndex = 10;
            this.button1.Text = "Oblicz";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TypeOfProfiles
            // 
            this.TypeOfProfiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TypeOfProfiles.FormattingEnabled = true;
            this.TypeOfProfiles.Location = new System.Drawing.Point(9, 80);
            this.TypeOfProfiles.MaxDropDownItems = 10;
            this.TypeOfProfiles.Name = "TypeOfProfiles";
            this.TypeOfProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TypeOfProfiles.Size = new System.Drawing.Size(121, 21);
            this.TypeOfProfiles.TabIndex = 60;
            this.TypeOfProfiles.SelectedIndexChanged += new System.EventHandler(this.TypeOfProfiles_SelectedIndexChanged);
            // 
            // selectSection
            // 
            this.selectSection.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectSection.FormattingEnabled = true;
            this.selectSection.Items.AddRange(new object[] {
            "IPE",
            "IPN",
            "HE",
            "UPN",
            "UPE"});
            this.selectSection.Location = new System.Drawing.Point(9, 40);
            this.selectSection.Name = "selectSection";
            this.selectSection.Size = new System.Drawing.Size(121, 21);
            this.selectSection.TabIndex = 62;
            this.selectSection.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "Profil stalowy:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Location = new System.Drawing.Point(153, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(180, 13);
            this.label16.TabIndex = 64;
            this.label16.Text = "Charakterystyki geometryczne:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 64);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 65;
            this.label17.Text = "Rodzaj grupy:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listView1);
            this.groupBox3.Controls.Add(this.ClassOfSteal);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.TypeOfProfiles);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.selectSection);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(15, 350);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(470, 171);
            this.groupBox3.TabIndex = 66;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Wybór profilu stalowego ";
            // 
            // listView1
            // 
            this.listView1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView1.Location = new System.Drawing.Point(149, 44);
            this.listView1.Name = "listView1";
            this.listView1.ShowItemToolTips = true;
            this.listView1.Size = new System.Drawing.Size(304, 97);
            this.listView1.TabIndex = 72;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // ClassOfSteal
            // 
            this.ClassOfSteal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClassOfSteal.FormattingEnabled = true;
            this.ClassOfSteal.Items.AddRange(new object[] {
            "S235",
            "S275",
            "S355"});
            this.ClassOfSteal.Location = new System.Drawing.Point(9, 120);
            this.ClassOfSteal.MaxDropDownItems = 10;
            this.ClassOfSteal.Name = "ClassOfSteal";
            this.ClassOfSteal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ClassOfSteal.Size = new System.Drawing.Size(121, 21);
            this.ClassOfSteal.TabIndex = 71;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 13);
            this.label18.TabIndex = 67;
            this.label18.Text = "Klasa stali:";
            // 
            // LabelMc
            // 
            this.LabelMc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LabelMc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelMc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelMc.Location = new System.Drawing.Point(320, 26);
            this.LabelMc.Name = "LabelMc";
            this.LabelMc.Size = new System.Drawing.Size(70, 20);
            this.LabelMc.TabIndex = 9;
            this.LabelMc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.White;
            this.progressBar1.Location = new System.Drawing.Point(173, 29);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(141, 15);
            this.progressBar1.TabIndex = 67;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(6, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(161, 15);
            this.label19.TabIndex = 68;
            this.label19.Text = "Wytężenie przy zginaniu";
            // 
            // Wyniki
            // 
            this.Wyniki.Controls.Add(this.label33);
            this.Wyniki.Controls.Add(this.label27);
            this.Wyniki.Controls.Add(this.LabelVc);
            this.Wyniki.Controls.Add(this.LabelSc);
            this.Wyniki.Controls.Add(this.progressBar3);
            this.Wyniki.Controls.Add(this.progressBar2);
            this.Wyniki.Controls.Add(this.label25);
            this.Wyniki.Controls.Add(this.label24);
            this.Wyniki.Controls.Add(this.label19);
            this.Wyniki.Controls.Add(this.LabelMc);
            this.Wyniki.Controls.Add(this.progressBar1);
            this.Wyniki.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Wyniki.Location = new System.Drawing.Point(346, 181);
            this.Wyniki.Name = "Wyniki";
            this.Wyniki.Size = new System.Drawing.Size(438, 163);
            this.Wyniki.TabIndex = 69;
            this.Wyniki.TabStop = false;
            this.Wyniki.Text = "Wyniki";
            // 
            // LabelVc
            // 
            this.LabelVc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LabelVc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelVc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelVc.Location = new System.Drawing.Point(320, 102);
            this.LabelVc.Name = "LabelVc";
            this.LabelVc.Size = new System.Drawing.Size(70, 20);
            this.LabelVc.TabIndex = 74;
            this.LabelVc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelSc
            // 
            this.LabelSc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LabelSc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelSc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelSc.Location = new System.Drawing.Point(320, 64);
            this.LabelSc.Name = "LabelSc";
            this.LabelSc.Size = new System.Drawing.Size(70, 20);
            this.LabelSc.TabIndex = 73;
            this.LabelSc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar3
            // 
            this.progressBar3.BackColor = System.Drawing.Color.White;
            this.progressBar3.ForeColor = System.Drawing.Color.White;
            this.progressBar3.Location = new System.Drawing.Point(173, 104);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(141, 15);
            this.progressBar3.TabIndex = 72;
            // 
            // progressBar2
            // 
            this.progressBar2.BackColor = System.Drawing.Color.White;
            this.progressBar2.ForeColor = System.Drawing.Color.White;
            this.progressBar2.Location = new System.Drawing.Point(173, 67);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(141, 15);
            this.progressBar2.TabIndex = 71;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(7, 102);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(160, 15);
            this.label25.TabIndex = 70;
            this.label25.Text = "Wytężenie przy ścinaniu";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(3, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(166, 15);
            this.label24.TabIndex = 69;
            this.label24.Text = "Wytężenie przy ściskaniu";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.textBoxNr);
            this.groupBox4.Controls.Add(this.textBoxVc);
            this.groupBox4.Controls.Add(this.textBoxNc);
            this.groupBox4.Controls.Add(this.textBoxMc);
            this.groupBox4.Controls.Add(this.richTextBoxNrd);
            this.groupBox4.Controls.Add(this.richTextBoxVrd);
            this.groupBox4.Controls.Add(this.richTextBoxNrds);
            this.groupBox4.Controls.Add(this.richTextBoxMcrd);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(491, 350);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(293, 171);
            this.groupBox4.TabIndex = 70;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nośność wybranego przekroju";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.Location = new System.Drawing.Point(233, 126);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 15);
            this.label31.TabIndex = 83;
            this.label31.Text = " [kN] ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.Location = new System.Drawing.Point(235, 92);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 15);
            this.label30.TabIndex = 82;
            this.label30.Text = " [kN] ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.Location = new System.Drawing.Point(235, 64);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 15);
            this.label29.TabIndex = 81;
            this.label29.Text = " [kN] ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.Location = new System.Drawing.Point(233, 35);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 15);
            this.label28.TabIndex = 10;
            this.label28.Text = " [kNm] ";
            // 
            // textBoxNr
            // 
            this.textBoxNr.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxNr.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNr.Location = new System.Drawing.Point(175, 118);
            this.textBoxNr.Name = "textBoxNr";
            this.textBoxNr.Size = new System.Drawing.Size(52, 23);
            this.textBoxNr.TabIndex = 80;
            // 
            // textBoxVc
            // 
            this.textBoxVc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxVc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxVc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxVc.Location = new System.Drawing.Point(175, 89);
            this.textBoxVc.Name = "textBoxVc";
            this.textBoxVc.Size = new System.Drawing.Size(52, 23);
            this.textBoxVc.TabIndex = 79;
            // 
            // textBoxNc
            // 
            this.textBoxNc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxNc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxNc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNc.Location = new System.Drawing.Point(175, 61);
            this.textBoxNc.Name = "textBoxNc";
            this.textBoxNc.Size = new System.Drawing.Size(52, 23);
            this.textBoxNc.TabIndex = 78;
            // 
            // textBoxMc
            // 
            this.textBoxMc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxMc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxMc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxMc.Location = new System.Drawing.Point(175, 32);
            this.textBoxMc.Name = "textBoxMc";
            this.textBoxMc.Size = new System.Drawing.Size(52, 23);
            this.textBoxMc.TabIndex = 77;
            // 
            // richTextBoxNrd
            // 
            this.richTextBoxNrd.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBoxNrd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxNrd.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBoxNrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxNrd.Location = new System.Drawing.Point(106, 121);
            this.richTextBoxNrd.Name = "richTextBoxNrd";
            this.richTextBoxNrd.Size = new System.Drawing.Size(63, 15);
            this.richTextBoxNrd.TabIndex = 76;
            this.richTextBoxNrd.Text = "";
            // 
            // richTextBoxVrd
            // 
            this.richTextBoxVrd.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBoxVrd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxVrd.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBoxVrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxVrd.Location = new System.Drawing.Point(106, 97);
            this.richTextBoxVrd.Name = "richTextBoxVrd";
            this.richTextBoxVrd.Size = new System.Drawing.Size(63, 15);
            this.richTextBoxVrd.TabIndex = 75;
            this.richTextBoxVrd.Text = "";
            // 
            // richTextBoxNrds
            // 
            this.richTextBoxNrds.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBoxNrds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxNrds.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBoxNrds.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxNrds.Location = new System.Drawing.Point(106, 64);
            this.richTextBoxNrds.Name = "richTextBoxNrds";
            this.richTextBoxNrds.Size = new System.Drawing.Size(63, 15);
            this.richTextBoxNrds.TabIndex = 74;
            this.richTextBoxNrds.Text = "";
            // 
            // richTextBoxMcrd
            // 
            this.richTextBoxMcrd.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBoxMcrd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxMcrd.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBoxMcrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxMcrd.Location = new System.Drawing.Point(106, 35);
            this.richTextBoxMcrd.Name = "richTextBoxMcrd";
            this.richTextBoxMcrd.Size = new System.Drawing.Size(63, 15);
            this.richTextBoxMcrd.TabIndex = 73;
            this.richTextBoxMcrd.Text = "";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(13, 126);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 15);
            this.label23.TabIndex = 72;
            this.label23.Text = "Rozciąganie";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(15, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 15);
            this.label22.TabIndex = 71;
            this.label22.Text = "Ścinanie";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(15, 69);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 15);
            this.label21.TabIndex = 70;
            this.label21.Text = "Ściskanie";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(15, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 15);
            this.label20.TabIndex = 69;
            this.label20.Text = "Zginanie";
            // 
            // textBox_S
            // 
            this.textBox_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_S.Location = new System.Drawing.Point(249, 132);
            this.textBox_S.Name = "textBox_S";
            this.textBox_S.Size = new System.Drawing.Size(40, 23);
            this.textBox_S.TabIndex = 11;
            this.textBox_S.Text = "0";
            this.textBox_S.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_S_KeyPress_1);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.Location = new System.Drawing.Point(742, 212);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 15);
            this.label26.TabIndex = 75;
            this.label26.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.Location = new System.Drawing.Point(396, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 15);
            this.label27.TabIndex = 76;
            this.label27.Text = "*";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.Location = new System.Drawing.Point(396, 102);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 15);
            this.label33.TabIndex = 77;
            this.label33.Text = "*";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(12, 528);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(461, 15);
            this.label34.TabIndex = 76;
            this.label34.Text = "* Nośność przekroju bez korelacji sił, wpływu zwichrzenia i wyboczenia";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(796, 552);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.Wyniki);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Belka stalowa - jednoprzęsłowa 1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Wyniki.ResumeLayout(false);
            this.Wyniki.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_L;
        private System.Windows.Forms.TextBox textBox_P;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_Q;
        private System.Windows.Forms.TextBox textBox_M;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox TypeOfProfiles;
        private System.Windows.Forms.ComboBox selectSection;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox ClassOfSteal;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label LabelMc;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox Wyniki;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RichTextBox richTextBoxMcrd;
        private System.Windows.Forms.RichTextBox richTextBoxNrd;
        private System.Windows.Forms.RichTextBox richTextBoxVrd;
        private System.Windows.Forms.RichTextBox richTextBoxNrds;
        private System.Windows.Forms.TextBox textBoxNr;
        private System.Windows.Forms.TextBox textBoxVc;
        private System.Windows.Forms.TextBox textBoxNc;
        private System.Windows.Forms.TextBox textBoxMc;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label LabelVc;
        private System.Windows.Forms.Label LabelSc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox_S;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label34;
    }
}

