﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace Nosnosc.Profiles
{
    public class ValueProfiles
    {
        public decimal G { get;  private set; }
        public decimal h { get; private set; }
        public decimal b { get; private set; }
        public decimal tw { get; private set; }
        public decimal tf { get; private set; }      
        public decimal A { get; private set; }   
        public decimal Wely { get; private set; }
        public decimal Wply { get; private set; }
        public decimal Ly { get; private set; }
        public decimal iy { get; private set; }
        public decimal Welz { get; private set; }
        public decimal Wplz { get; private set; }
        public decimal Lz { get; private set; }
        public decimal iz { get; private set; }
        public string ID { get; private set; }
        public static string section { get; set;}

        public static string GetSelect()
        {
            return @"SELECT [G]
                          ,[h]
                          ,[b]
                          ,[tw]
                          ,[tf]                     
                          ,[A]
                          ,[Wely]
                          ,[Wply]
                          ,[Ly]
                          ,[iy]
                          ,[Welz]
                          ,[Wplz]
                          ,[Lz]
                          ,[iz]
                          ,[ID]
                      FROM [dbo].["+section+"]";
        }

        public ValueProfiles(SqlDataReader reader)
        {
            this.G = Convert.ToDecimal(reader["G"]);
            this.h = Convert.ToDecimal(reader["h"]);
            this.b = Convert.ToDecimal(reader["b"]);
            this.tw = Convert.ToDecimal(reader["tw"]);
            this.tf = Convert.ToDecimal(reader["tf"]);        
            this.A = Convert.ToDecimal(reader["A"]);
            this.Wely = Convert.ToDecimal(reader["Wely"]);
            this.Wply = Convert.ToDecimal(reader["Wply"]);
            this.Ly = Convert.ToDecimal(reader["Ly"]);
            this.iy = Convert.ToDecimal(reader["iy"]);      
            this.Welz = Convert.ToDecimal(reader["Welz"]);
            this.Wplz = Convert.ToDecimal(reader["Wplz"]);
            this.Lz = Convert.ToDecimal(reader["Lz"]);
            this.iz = Convert.ToDecimal(reader["iz"]);
            this.ID = Convert.ToString(reader["ID"]);
         
        }


        #region Pobranie listy profili z bazy 
        public static List<string> GetIPNs(SqlConnection connection)
        {
            List<string> profiles = new List<string>();
            string sql = "select ID from "+ section +"";

            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    profiles.Add((reader[0] as string).Trim());
            }  
                return profiles;
        }
        #endregion

        public ListViewItem[] ToArray()
        {
            string zmienna = this.ToString().Replace("  ", " ");
            string[] tablice = zmienna.Split(new char[] { '\t', '\n' });

            ListViewItem[] tab = new ListViewItem[tablice.Length];
            for (int i = 0; i < tab.Length; i++)
                tab[i] = nazwa(tablice[i]);

            IEnumerable<ListViewItem> col = tablice.Select(i => new ListViewItem(i));

            return tablice.Select(i => new ListViewItem(i)).ToArray();
        }

        public ListViewItem nazwa (string i)
        {
            return new ListViewItem(i);
        }


        public override string ToString()
        {
            return string.Format("G: {0}kg/m  \t h: {1}mm  \n b: {2}mm \t tw: {3}mm \n tf: {4}mm \t A: {5} \n Wely: {6} \t Wply: {7} \n Ly: {8} \t iy: {9} \n Welz: {10} \t Wplz: {11} \n Lz: {12} \t iz:{13}"
                          , this.G , this.h, this.b, this.tw, this.tf, this.A, this.Wely, this.Wply, this.Ly, this.iy, this.Welz, this.Wplz, this.Lz, this.iz);
        }

        public static ValueProfiles GetIPNbyIPN(SqlConnection connection, string s)//ip4500
        {//select .... from where id like '%ip4500%'
            string sqla = string.Format("{0} WHERE ID like '%{1}%'", GetSelect(), s);
            using (SqlCommand cmd = new SqlCommand(sqla, connection))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    return new ValueProfiles(reader);
            }
            return null;
        }

    }
}
