﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nosnosc.Utils
{
    public enum Opcje
    {
        Sila_skupiona = 1,
        Sila_rozłożona = 2,
        Po_trojkacie = 3,
        Moment = 4
    }
}
